
var myWishList = {
	bills : []
};

function newWishList(object)
{
	function F() {}
    F.prototype = object;
    return new F();
};

myWishList.addItem = function(item)
{
	var restaurantIndex = this.isRestaurantExist(item.name);
	//case restaurant doesn't exist 
	if(restaurantIndex == -1) {
		var b = newBill(bill);
		//to don't get copy table items from the previous
		b.items = [];
		b.restaurantName = item.name;
		b.htmlId = "b" + item.restaurant_id;
		b.restaurantId = item.restaurant_id;
		b.tabIndex = this.bills.length;
		b.total = item.price;
		b.descriptionRestaurant = item.restaurant_description;
		b.lat = item.lat;
		b.long = item.lng;
		b.rating = Math.round(item.sum_rate/item.nb_rate);
		b.sum_rate=item.sum_rate;
		b.nb_rate=item.nb_rate;
		var itemToAdd = newItem(item);
		itemToAdd.id = item.id;
		itemToAdd.name = item.item;
		itemToAdd.urlImage = item.image_url;
		itemToAdd.price = item.price;
		itemToAdd.restaurantName = item.name;
		itemToAdd.nbItem = 1;
		itemToAdd.htmlId = "i" + item.id;
		itemToAdd.tabIndex = 0;
		b.addItem(itemToAdd);
		this.bills.push(b);
		return [b, itemToAdd];
	} else {
		var itemIndex = this.bills[restaurantIndex].isItemExist(item.id);
		//case item doesn't exist
		if( itemIndex == -1) {
			var itemToAdd = newItem(item);
			itemToAdd.id = item.id;
			itemToAdd.name = item.item;
			itemToAdd.urlImage = item.image_url;
			itemToAdd.restaurantName = item.name;
			itemToAdd.price = item.price;
			itemToAdd.nbItem = 1;
			itemToAdd.tabIndex = this.bills[restaurantIndex].items.length;
			itemToAdd.htmlId = "i" + item.id;
			this.bills[restaurantIndex].total+=item.price;
			//round total and get two numbers after comma
			this.bills[restaurantIndex].total = (Math.round(this.bills[restaurantIndex].total * 100)) / 100;
			this.bills[restaurantIndex].addItem(itemToAdd);
			return [this.bills[restaurantIndex], itemToAdd];
		} else {
			this.bills[restaurantIndex].items[itemIndex].nbItem++;
			this.bills[restaurantIndex].total+=this.bills[restaurantIndex].items[itemIndex].price;
			this.bills[restaurantIndex].total = (Math.round(this.bills[restaurantIndex].total * 100)) / 100;
			return [this.bills[restaurantIndex], this.bills[restaurantIndex].items[itemIndex]];
		}
	}
		
}

myWishList.isRestaurantExist = function(restaurantName)
{
	for (var i = 0; i < this.bills.length; i++)
	{
		// this.bills[i] !=0 used because compare with object
		if (this.bills[i] != 0 && this.bills[i].restaurantName == restaurantName){
			return i;
		}
	}
	return -1;
}
