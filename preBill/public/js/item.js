
var item = {
	id : "",
	name : "",
	urlImage : "",
	restaurantName : "",
	price : "",
	nbItem : "",
	htmlId : "",
	tabIndex: 0
};

function newItem(object)
{
	function F() {}
	F.prototype = object;
	return new F();
}

item.setId = function(id) 
{
	this.id = id;
}

newItem.getId = function()
{
	return this.id;
}

item.setName = function(name)
{
	this.name = name;
}

item.setUrlImage = function(urlImage)
{
	this.urlImage = urlImage;
}

item.setRestaurantName = function(restaurantName)
{
	this.restaurantName = restaurantName;
}

item.setPrice = function(price)
{
	this.price = price;
}

item.setNbItem = function(nbItem)
{
	this.nbItem = nbItem;
}

item.getName = function()
{
	return this.name;
}

item.getUrlImage = function()
{
	return this.urlImage;
}

item.getRestaurantName = function()
{
	return this.restaurantName;
}

item.getPrice = function()
{
	return this.price;
}

item.getNbItem = function()
{
	return this.nbItem;
}
