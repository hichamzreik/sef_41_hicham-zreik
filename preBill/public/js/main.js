
function clickLess(e)
{
    $(e).closest(".full-text").css( "display", "none" );
    $(e).parent().prev().css( "display", "block" );
} 

function clickMore(e) 
{
    $(e).closest(".less-text").css( "display", "none" );
    $(e).parent().next().css( "display", "block" );
} 

function enableDiv(idDiv)
{
    document.getElementById(idDiv).style.pointerEvents = 'auto';
    document.getElementById(idDiv).style.color = '#636b6f';
}

function disableDiv(idDiv)
{
  document.getElementById(idDiv).style.pointerEvents = 'none';
  document.getElementById(idDiv).style.color = '#C3645F';
}

//we use this table when we add item we set the in this table and we send via that
var dataItems = [];

function categoryClicked(categoryName){
  var item = {'data' : categoryName,};
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  })
  $.ajax({
    url: 'items',
    type: 'POST',
    processData: false,
    data:JSON.stringify(item) ,
    dataType: 'JSON',
    success: function (data) {
    $("#mid_col_0").html("");
    $("#mid_col_1").html("");
    $("#mid_col_2").html("");
    enableDiv("salads");
    enableDiv("desserts");
    enableDiv("beverages");
    enableDiv("pizzas");
      dataItems = data.results;
      for(i=0;i<data.results.length;i++)
      {   
          //to disable button less and more if the description is small
          if(data.results[i].description.length<=29) {
            var v = i%3;
            $("#mid_col_"+v).append("<div class=\"panel panel-default\">"
              + "<div class=\"panel-body\">"
              +"<img  style=\" height:100px;\" src=\"img/" + data.results[i].image_url+"\"/> &nbsp;&nbsp;"
              + "<FONT COLOR=\"#000000\">"+data.results[i].item + "</FONT>"
              + "<br/>"

              + "<span>"+data.results[i].description + "</span>"
              + "<br/>"

              + "<FONT COLOR=\"#000000\">Restaurant : </FONT><FONT COLOR=\"#C3645F\">"
              + data.results[i].name+"</FONT><br/>"
              + "<FONT COLOR=\"#000000\">Price : </FONT><FONT COLOR=\"#C3645F\">"+data.results[i].price +"$</FONT><br/>"
              + "<button class=\"button col-md-offset-9 btn btn-danger\" style=\" background-color:#C3645F\" onclick = \"addWishList("+ i +")\">ADD</button>"
              + "</div></div>"
              );
          } else {
            var v = i%3;
            $("#mid_col_"+v).append("<div class=\"panel panel-default\">"
              + "<div class=\"panel-body\">"
              +"<img  style=\" height:100px;\" src=\"img/" + data.results[i].image_url+"\"/> &nbsp;&nbsp;"
              + "<FONT COLOR=\"#000000\">"+data.results[i].item + "</FONT>"
              + "<br/>"

              + "<div class=\" less-text \"> "
              + "<span>"+data.results[i].description.trunc(30) + "</span>"
              + "<span class=\" more-button \" onclick=\" clickMore(this) \" > &nbsp; more </span>"
              + "</div> "

              + "<div class=\" full-text \"> "
              + "<span>"+data.results[i].description + "</span>"
              + "<span class=\" less-button \" onclick=\" clickLess(this) \"> &nbsp; less </span>"
              + "</div> "
              + "<br/>"

              + "<FONT COLOR=\"#000000\">Restaurant : </FONT><FONT COLOR=\"#C3645F\">"
              + data.results[i].name+"</FONT><br/>"
              + "<FONT COLOR=\"#000000\">Price : </FONT><FONT COLOR=\"#C3645F\">"+data.results[i].price +"$</FONT><br/>"
              + "<button class=\"button col-md-offset-9 btn btn-danger\" style=\" background-color:#C3645F\" onclick = \"addWishList("+ i +")\">ADD</button>"
              + "</div></div>"
              );
          }
      }
      disableDiv(String(categoryName));
    }, error:function(error) {},
  });
}

function increaseWishList(billWishlistId,itemWishlistId)
{
  wishList.bills[billWishlistId].items[itemWishlistId].nbItem++;
  wishList.bills[billWishlistId].total = wishList.bills[billWishlistId].total + wishList.bills[billWishlistId].items[itemWishlistId].price;
  wishList.bills[billWishlistId].total = (Math.round(wishList.bills[billWishlistId].total * 100)) / 100;
  $("#"+ wishList.bills[billWishlistId].items[itemWishlistId].htmlId + "QT").html(wishList.bills[billWishlistId].items[itemWishlistId].nbItem);
  var m = wishList.bills[billWishlistId].items[itemWishlistId].nbItem * wishList.bills[billWishlistId].items[itemWishlistId].price;
  m = (Math.round(m* 100)) / 100;
  $("#"+ wishList.bills[billWishlistId].items[itemWishlistId].htmlId + "PT").html(m+"$");
  $("#"+ wishList.bills[billWishlistId].htmlId + "Total").html(wishList.bills[billWishlistId].total + "$");
}

function decreaseWishList(billWishlistId,itemWishlistId)
{
  wishList.bills[billWishlistId].total = wishList.bills[billWishlistId].total - wishList.bills[billWishlistId].items[itemWishlistId].price;
  wishList.bills[billWishlistId].total = (Math.round(wishList.bills[billWishlistId].total * 100)) / 100;
  wishList.bills[billWishlistId].items[itemWishlistId].nbItem--;
  if(wishList.bills[billWishlistId].items[itemWishlistId].nbItem != 0) {
    $("#"+ wishList.bills[billWishlistId].items[itemWishlistId].htmlId + "QT").html(wishList.bills[billWishlistId].items[itemWishlistId].nbItem);
    var m = wishList.bills[billWishlistId].items[itemWishlistId].nbItem * wishList.bills[billWishlistId].items[itemWishlistId].price;
    m = (Math.round(m* 100)) / 100;
    $("#"+ wishList.bills[billWishlistId].items[itemWishlistId].htmlId + "PT").html(m+"$");
    $("#"+ wishList.bills[billWishlistId].htmlId + "Total").html(wishList.bills[billWishlistId].total + "$");
  } else {
    deleteFromWishList(billWishlistId,itemWishlistId);
  }
}

function deleteFromWishList(billWishlistId,itemWishlistId)
{
  wishList.bills[billWishlistId].total -=  wishList.bills[billWishlistId].items[itemWishlistId].price * wishList.bills[billWishlistId].items[itemWishlistId].nbItem;
  wishList.bills[billWishlistId].total = (Math.round(wishList.bills[billWishlistId].total * 100)) / 100;
  var parent = document.getElementById(wishList.bills[billWishlistId].htmlId+"Body");
  $("#"+ wishList.bills[billWishlistId].htmlId + "Total").html(wishList.bills[billWishlistId].total + " $");
  parent.removeChild(document.getElementById(wishList.bills[billWishlistId].items[itemWishlistId].htmlId));
  wishList.bills[billWishlistId].items[itemWishlistId] = 0;
  for (var i = 0; i < wishList.bills.length; i++)
  {
    if (wishList.bills[i] != 0) {
        var j =0;
        for (; j < wishList.bills[i].items.length; j++)
        {
          if (wishList.bills[i].items[j]!=0) {
            break;
          }
        }
        if (j == wishList.bills[i].items.length) {
          var parent = document.getElementById("billsDiv");
          parent.removeChild(document.getElementById(wishList.bills[billWishlistId].htmlId));
          wishList.bills[billWishlistId]=0;
        }
    }
  }
}

var wishList = newWishList(myWishList);

function addWishList(index)
{
  //get the item to add
  var selectItem = dataItems[index];
  var tabBillItem = wishList.addItem(selectItem);
  showItems(tabBillItem[0],tabBillItem[1]);
  $("#"+ tabBillItem[0].htmlId + "Total").html(tabBillItem[0].total + "$");
}

function showItems(bill, item)
{
  if(bill.items.length == 1 && item.nbItem == 1) {
    $("#billsDiv").append(
        "<div id=\"" + bill.htmlId + "\" class=\"panel panel-default\">"
        +"<div id=\"" + bill.htmlId + "Body\" class=\"panel-body\" >"
        +"<div class=\"row\">"
        +"<div class=\"col-md-4\">"
        + "<FONT class=\"wishlist-row wishList-restaurant \" COLOR=\"#C3645F\">" 
        + "<div  onclick=\"openGallery("+bill.tabIndex+")\">"
        + bill.restaurantName +"</div></FONT>"
        + "</div>"
        + "<div class=\"col-md-2 col-md-offset-4\" ><img class=\"button-gallery\" src=\"img/gallery.png\"  onclick=\"openGallery("+bill.tabIndex+")\"></div>"
        + "<div class=\"col-md-2\" ><img class=\"button-map\" src=\"img/map.png\" data-toggle=\"modal\" data-target=\"#favoritesModal\" onclick=\"showMap("+bill.tabIndex+")\"></div>"
        +"</div>"
        + "<div class=\"row wishlist-row\">"
        + "<div  class=\"col-md-4 wishList-item \">Item</div>"
        + "<div class=\"col-md-2 wishList-item\">Price</div>"
        + "<div class=\"col-md-3 wishList-item \">Qt</div>" 
        + "<div class=\"col-md-3 wishList-item \">Total</div>"
        + "</div>"
        + "<div class=\"row wishlist-row \" id=\""+ item.htmlId +"\">"
        + "<div  class=\"col-md-4 name-item\" id=\""+ item.htmlId +"Name\">" + item.name + "</div>"
        + "<div class=\"col-md-2 all-price \" id=\""+ item.htmlId +"P\">" + item.price + "$</div>"
        + "<div class=\"col-md-3  \">"
        + "<img  class=\"img-plus-minus\" src=\"img/plus.png \" onclick=\"increaseWishList(" + bill.tabIndex + "," + item.tabIndex + ")\" />" 
        + "<div class=\"qt\" id=\""+ item.htmlId +"QT\">1</div>" 
        + "<img  class=\"img-plus-minus\" src=\"img/minus.png \" onclick=\"decreaseWishList(" + bill.tabIndex + "," + item.tabIndex + ")\" />"
        +"</div>"
        + "<div class=\"col-md-3  \">"
        + "<div class=\"row\"><div class=\"col-md-6 all-price \" style=\"display: inline-block;float:left;\" id=\""+ item.htmlId +"PT\">" + item.price + "$</div>"
        + "<div class=\"col-md-6\"><img src=\"img/delete.png\" class=\"delete\" onclick=\"deleteFromWishList(" + bill.tabIndex + "," + item.tabIndex + ")\" />"
        + "</div></div></div>"
        + "</div>"
        + "</div>"
        + "<div class=\"row wishlist-row \">"
        + "<div class=\"col-md-7\"></div>"
        + "<div class=\"col-md-2 wishList-item \">Total </div>" 
        + "<div class=\"col-md-3  all-price\" id=\"" + bill.htmlId + "Total\">"+item.price+"$</div>"
        + "</div>"
        +"</div>");
  } else {
      if (item.nbItem == 1) {
        $("#"+bill.htmlId + "Body" ).append(
          "<div class=\"row wishlist-row\"id=\""+ item.htmlId +"\">"
            + "<div  class=\"col-md-4  name-item\" id=\""+ item.htmlId +"Name\">" + item.name + "</div>"
            + "<div class=\"col-md-2 all-price \" id=\""+ item.htmlId +"P\">" + item.price + "$</div>"
            + "<div class=\"col-md-3  \">"
            + "<img class=\"img-plus-minus\" src=\"img/plus.png \" onclick=\"increaseWishList(" + bill.tabIndex + "," + item.tabIndex + ")\" />" 
            + "<div class=\"qt\" id=\""+ item.htmlId +"QT\">1</div>" 
            + "<img class=\"img-plus-minus\" src=\"img/minus.png \" onclick=\"decreaseWishList(" + bill.tabIndex + "," + item.tabIndex + ")\" /></div>"
            + "<div class=\"col-md-3  \">"
            + "<div class=\"row\"><div class=\"col-md-6 all-price\" style=\"display: inline-block;float:left;\" id=\""+ item.htmlId +"PT\">" + item.price + "$ </div>"
            + "<div class=\"col-md-6\"><img src=\"img/delete.png\" class=\"delete\" onclick=\"deleteFromWishList(" + bill.tabIndex + "," + item.tabIndex + ")\" />"
            + "</div></div></div>"
          + "</div>"
          );
      } else {
        $("#"+item.htmlId + "QT").html(item.nbItem);
        var m = item.nbItem * item.price;
        m = (Math.round(m* 100)) / 100;
        $("#"+item.htmlId + "PT").html(m+"$");
      }
  }
}

function showMap(index)
{
  var lat = wishList.bills[index].lat;
  var long = wishList.bills[index].long;
  //need to stop javascript 1 s because javascript fast than html in load
  var inter = setInterval(function(){ var mapOptions = {
      center: new google.maps.LatLng(lat, long),
      zoom: 18,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    var map = new google.maps.Map($("#dvMap")[0], mapOptions); 
    $("#favoritesModalLabel").html("<FONT class=\"word-categorie\" color=\"#C3645F\">"+wishList.bills[index].restaurantName+"</FONT>");
    $("#descriptionRestaurant").html(wishList.bills[index].descriptionRestaurant);
    var destPosition= {lat : lat, lng : long};
    var infoWindowOrigin = new google.maps.InfoWindow({map: map});
    infoWindowOrigin.setPosition(destPosition);
    infoWindowOrigin.setContent(wishList.bills[index].restaurantName);
    var rate = document.getElementById('rating');
    rate.innerHTML="";
    var data = { rating: wishList.bills[index].rating };
    addRatingWidget(buildShopItem(rate, data), data,index);
    clearInterval(inter);
  }, 1000);
}

function buildShopItem(rate, data)
{
  var shopItem = document.createElement('div');
  var html = '<div class="c-shop-item__img"></div>' +
  '<div class="c-shop-item__details">' +
  '<ul class="c-rating"></ul>' +
  '</div>';
  shopItem.classList.add('c-shop-item');
  shopItem.innerHTML = html;
  rate.appendChild(shopItem);
  return shopItem;
}

function addRatingWidget(shopItem, data,index)
{
  var ratingElement = shopItem.querySelector('.c-rating');
  var currentRating = data.rating;
  var maxRating = 5;
  var callback = function(rating)
  { 
    var newSumRate = wishList.bills[index].sum_rate+rating;
    var newNbRate = wishList.bills[index].nb_rate+1;
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
    var dataToSend = {
      'sumRate': newSumRate,
      'nbRate': newNbRate,
      'restaurantId' : wishList.bills[index].restaurantId,
    }
    $.ajax({
      type:'POST',
      url:'addRate',
      contentType: "json",
      processData: false,
      data:JSON.stringify(dataToSend),
      success:function(data){
        
      }, error:function(error) {
        console.log("error");
      },
    });
      alert("Thank you for your rating!"); 
  };
  var r = rating(ratingElement, currentRating, maxRating, callback);
}

function openGallery(index) {
  var pswpElement = document.querySelectorAll('.pswp')[0];
  var items = [
    {
      src: 'img/'+wishList.bills[index].restaurantName+'/img1.jpg',
      w: 964,
      h: 1024
    },
    {
      src: 'img/'+wishList.bills[index].restaurantName+'/img2.jpg',
      w: 1024,
      h: 683
    },
    {
      src: 'img/'+wishList.bills[index].restaurantName+'/img3.jpg',
      w: 1024,
      h: 683
    },
    {
      src: 'img/'+wishList.bills[index].restaurantName+'/img4.jpg',
      w: 1024,
      h: 683
    }
  ];
  var options = {
    history: false,
    focus: false,
    showAnimationDuration: 0,
    hideAnimationDuration: 0
  };
  var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
  gallery.init();
};

$(document).ready(function() {

  String.prototype.trunc = String.prototype.trunc ||
        function(n){
            return (this.length > n) ? this.substr(0,n-1)+'&hellip;' : this;
        };

  //Search items
   $('#searchInput').on('input',function(e){
      enableDiv("salads");
      enableDiv("desserts");
      enableDiv("beverages");
      enableDiv("pizzas");
      var textToSearch = $("#searchInput").val();
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      })
      var dataToSend = {
          'text': textToSearch,
      }
      if(textToSearch.length>=3) {

        $.ajax({
          type:'post',
          url:'search',
          contentType: "json",
          processData: false,
          data:JSON.stringify(dataToSend),
          success:function(data){
            $("#mid_col_0").html("");
            $("#mid_col_1").html("");
            $("#mid_col_2").html("");
            enableDiv("salads");
            enableDiv("desserts");
            enableDiv("beverages");
            enableDiv("pizzas");
            dataItems = data.results;
            for(i=0;i<data.results.length;i++)
            {
                //to mark text searched
                var textToMark = textToSearch;
                var result=data.results[i].item;
                //gi to ignore lower and capital lettre
                var reg = new RegExp(textToMark, 'gi');
                var markText = result.replace(reg, function(str) {return '<FONT style="font-size:22px" COLOR="#ff0000"><b>'+str+'</b></FONT>'});
                data.results[i].item = markText;
                var textToMark = textToSearch;
                var result=data.results[i].description;
                var reg = new RegExp(textToMark, 'gi');
                var markText = result.replace(reg, function(str) {return '<FONT style="font-size:22px" COLOR="#ff0000">'+str+'</FONT>'});
                data.results[i].description = markText;
                var v = i%3;
                $("#mid_col_"+v).append("<div class=\"panel panel-default\">"
                  + "<div class=\"panel-body\">"
                  +"<img  style=\" height:100px;\" src=\"img/" + data.results[i].image_url+"\"/> &nbsp;&nbsp;"
                  + "<FONT COLOR=\"#000000\">"+data.results[i].item + "</FONT>"
                  + "<br/>"

                  + "<span>"+data.results[i].description + "</span>"
                  + "<br/>"

                  + "<FONT COLOR=\"#000000\">Restaurant : </FONT><FONT COLOR=\"#C3645F\">"
                  + data.results[i].name+"</FONT><br/>"
                  + "<FONT COLOR=\"#000000\">Price : </FONT><FONT COLOR=\"#C3645F\">"+data.results[i].price +"$</FONT><br/>"
                  + "<button class=\"button col-md-offset-9 btn btn-danger\" style=\" background-color:#C3645F\" onclick = \"addWishList("+ i +")\">ADD</button>"
                  + "</div></div>"
                  ); 
            }
          }, error:function(error) {
                   console.log("error");
              },
        });
      } else {
          $("#mid_col_0").html("");
          $("#mid_col_1").html("");
          $("#mid_col_2").html("");
      }
  });   

});
