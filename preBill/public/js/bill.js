
var bill = {
	restaurantName : "",
	restaurantId:"",
	items : [],
	total : "",
	htmlId : "",
	tabIndex: 0,
	descriptionRestaurant: "",
	lat: 0,
	long: 0,
	nb_rate: 0,
	sum_rate: 0,
	rating: 0
};

function newBill(object) {
	function F() {}
	F.prototype = object;
	return new F();
}

bill.isItemExist = function (idItem) {
	for(var i=0;i<this.items.length;i++) {
		if(this.items[i] != 0 && this.items[i].id == idItem) {
			return i;
		}
	}
	return -1;
}

bill.addItem = function(item) {
	this.items.push(item);
}

bill.setName = function(name) {
	this.name = name;
}

bill.setTotal = function(total) {
	this.total = total;
}

bill.getName = function(){
	return this.name;
}

bill.getTotal = function(){
	return this.total;
}

bill.getRestaurantName = function(){
	return this.restaurantName;
}