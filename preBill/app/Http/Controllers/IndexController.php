<?php

namespace preBill\Http\Controllers;

use Illuminate\Http\Request;

use preBill\Http\Requests;

use Illuminate\Support\Facades\DB;

use preBill\Item;

use preBill\Categorie;

use preBill\Restaurant;

class IndexController extends Controller
{
    public function index()
    {
    	return view('index');
    }

    public function getItems(Request $request)
    {
    	$data = json_decode($request->getContent(),true);
        $items = DB::table('items')
        ->join('categories', 'categories.id', '=', 'items.type')
        ->join('restaurants', 'restaurants.id', '=', 'items.restaurant_id')
        ->where('categories.type', '=', $data)
        ->select('items.id', 'items.image_url', 'items.item', 'items.description','items.price','items.restaurant_id','restaurants.name','restaurants.restaurant_description','restaurants.lat','restaurants.lng','restaurants.sum_rate','restaurants.nb_rate')
        ->get();
        return response()->json(array('results'=> $items), 200);
    }

    public function search(Request $request) 
    {
        $data = json_decode($request->getContent(),true);
        $txt = $data['text'];
        $items = DB::table('items')
        ->join('restaurants', 'restaurants.id', '=', 'items.restaurant_id')
        ->where('items.description', 'LIKE', '%'.$txt.'%')
        ->orWhere('item', 'LIKE', '%'.$txt.'%')
        ->select('items.id', 'items.image_url', 'items.item', 'items.description','items.price','items.restaurant_id','restaurants.name','restaurants.restaurant_description','restaurants.lat','restaurants.lng','restaurants.sum_rate','restaurants.nb_rate')
        ->get();
        return response()->json(array('results'=> $items), 200);
    }

    public function addRate(Request $request) 
    {
        $data = json_decode($request->getContent(),true);
        $restaurantId = $data['restaurantId'];
        $sumRate = $data['sumRate'];
        $nbRate = $data['nbRate'];
        $restaurant = Restaurant::find($restaurantId);
        //Update without touching timestamps
        $restaurant->timestamps = false;
        $restaurant->nb_rate = $nbRate;
        $restaurant->sum_rate = $sumRate;
        $restaurant->save();
        return response()->json(array('restaurant'=> $data), 200);
    }

}
