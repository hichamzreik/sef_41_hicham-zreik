<?php

namespace preBill;

use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{
    protected $table = "categories";

    public function items()
   {
       return $this->hasMany('preBill\Item');
   }
}
