<?php

namespace preBill;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = "items";

    public function categories() 
    {
        return $this->belongsTo('preBill\Categorie');
    }
}
