<?php

namespace preBill;

use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{
	public function items() 
    {
    	return $this->hasMany('preBill\Item');
	}
}
