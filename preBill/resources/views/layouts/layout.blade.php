<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name', 'Laravel') }}</title>

    <link href="{{ url('/css/app.css') }}" rel="stylesheet">
    <link href="{{ url('/css/main.css') }}" rel="stylesheet">

    <!-- css for rating -->
    <link rel="stylesheet" href="{{ url('/css/rating.min.css') }}">

    <!-- css for gallery -->
    <link rel="stylesheet" href="{{ url('/css/photoswipe.min.css') }}">
    <link rel="stylesheet" href="{{ url('/css/default-skin.min.css') }}">

    <!-- script for gallery -->
    <script src="{{ url('/js/photoswipe.min.js') }}"></script>
    <script src="{{ url('/js/photoswipe-ui-default.min.js') }}"></script>

    <!-- script for rating -->
    <script src="{{ url('/js/rating.min.js') }}"></script>

    <!-- script for map -->
     <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAxA1C4X_WW26vJhm0PGNtQ-xSiSgzUZGE">
    </script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.1.0.min.js"   integrity="sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s="   crossorigin="anonymous"></script>
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>

    <!-- Top bar -->
    <nav  class="panel panel-default navbar navbar-default navbar-fixed-top nav-top" >
        <div style="margin-top:3px" class="row">
            <div class="col-md-3">
                <img src = "img/logo.png" class="logo" /><span class="word-logo">Shawkeh w skeneh </span>
            </div>
            <div class="col-md-5">
                <input style="margin-top:6px" type="text" class="form-control" id="searchInput" placeholder="Search for a dish..." />
            </div>
            <div class="col-md-4"></div>
        </div>
    </nav>

    @yield('content')
    
    <meta name="csrf-token" content="{!! csrf_token() !!}">
    <!-- Scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="{{ url('/js/app.js') }}"></script>
    <script src="{{ url('/js/wishList.js') }}"></script>
    <script src="{{ url('/js/bill.js') }}"></script>
    <script src="{{ url('/js/item.js') }}"></script>
    <script src="{{ url('/js/main.js') }}"></script>
</body>
</html>