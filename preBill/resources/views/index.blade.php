@extends('layouts.layout')

@section('content')

<div class="row  vert-offset-top-6">

    <!-- Left Side -->
    <div class="col-md-2">
        <div class="panel panel-default col-md-offset-1">
            <div class="panel-body">
                <p class="col-md-offset-1 word-categorie"><FONT COLOR="#C3645F">Categories</FONT></p>
                <p class="col-md-offset-2 cursorMouse word-item" id="pizzas" onclick="categoryClicked('pizzas')">Pizzas
                </p>
                <p class="col-md-offset-2 cursorMouse word-item" id="salads" onclick="categoryClicked('salads')">Salads
                </p>
                <p class="col-md-offset-2 cursorMouse word-item" id="desserts" onclick="categoryClicked('desserts')">Desserts
                </p>
                <p class="col-md-offset-2 cursorMouse word-item" id="beverages" onclick="categoryClicked('beverages')">Beverages
                </p>
            </div>
        </div>
    </div>

    <!-- body Side -->
    <div class="col-md-7">
        <div class="row">
            <div id="mid_col_0" class="col-md-4">
                <div class="row">
                    <div class="col-md-5 col-md-offset-2">
                        <FONT class="title" COLOR=#C3645F>Craving</FONT>
                    </div>
                </div>
                <div class="panel panel-default vert-offset-top-1">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-11 col-md-offset-1 word">
                                Pizza time
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-10 col-md-offset-2">
                                <img class="image-center" src="img/pizza.png"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1 phrase">
                                Meaty, cheesy, delicious. Just about the perfect food
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default ">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-11 col-md-offset-1 word">
                                Super sandwiches
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-10 col-md-offset-2">
                                <img class="image-center" src="img/sandwish.png"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1 phrase">
                               Two slices of bread, meat and veggies and the rest is mere details
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="mid_col_1" class="col-md-4">
                <div class="row">
                    <div class="col-md-5 col-md-offset-2">
                        <FONT class="title" COLOR=#C3645F>something</FONT>
                    </div>
                </div> 
                <div class="panel panel-default vert-offset-top-1">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-11 col-md-offset-1 word">
                                Salad bars
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-10 col-md-offset-2">
                                <img class="image-center" src="img/salad.png"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1 phrase">
                                Fill yourself up the healthy way
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default ">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-11 col-md-offset-1 word">
                                A well known beverage restaurants
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-10 col-md-offset-2">
                                <img class="image-center" src="img/juice.png"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1 phrase">
                               Freshly made fruit and vegetable juices
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="mid_col_2" class="col-md-4">
                <div class="row">
                    <div class="col-md-5 col-md-offset-2">
                        <FONT class="title" COLOR=#C3645F>particular?</FONT>
                    </div>
                </div>
                <div class="panel panel-default vert-offset-top-1">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-11 col-md-offset-1 word">
                                Sweet tooth
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-10 col-md-offset-2">
                                <img class="image-center" src="img/dessert.png"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1 phrase">
                                Life is short so eat dessert first!
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 vert-offset-top-5">
                        <FONT class="corner" COLOR=#C3645F> Let's help you find them!</FONT>
                    </div>
                </div> 
            </div>
        </div>
    </div>

    <!-- Right Sid -->
    <div class="col-md-3" id="wishListDiv">
        <div class="panel panel-default">
            <div class="panel-body" >
                   <div class="row">
                        <div class="col-md-8 col-md-offset-4 word-wishList"><FONT COLOR="#C3645F">Wish List</FONT></div>
                   </div>
            </div>
        </div>
        <div id="billsDiv"></div>
    </div>

</div>

<!-- when we pressed on the icon  -->
<div class="modal fade" id="favoritesModal" tabindex="-1" role="dialog"  aria-labelledby="favoritesModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="favoritesModalLabel">
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-11 map" id="dvMap" ></div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-2" style="text-align:left;"><FONT COLOR="#C3645F" style="text-size: 22px">       Description</FONT>
                    </div>
                    <div style="text-align:left;" class="col-md-10" id="descriptionRestaurant"></div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-2" style="text-align:left;" ><FONT COLOR="#C3645F" style="text-size: 22px">  Rating</FONT>
                        </div>
                            <div style="text-align:left;" class="col-md-10"><div id="rating"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 col-md-offset-9">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- gallery -->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true" style="">
    <div class="pswp__bg"></div>
    <div class="pswp__scroll-wrap">
        <div class="pswp__container" style="transform: translate3d(-806px, 0px, 0px);">
            <div class="pswp__item" style="display: block; transform: translate3d(1612px, 0px, 0px);"></div>
            <div class="pswp__item" style="transform: translate3d(0px, 0px, 0px);">
                <div class="pswp__zoom-wrap" style="transform: translate3d(134px, 44px, 0px) scale(1);">
                    <div class="pswp__img pswp__img--placeholder pswp__img--placeholder--blank" style="width: 452px; height: 480px; display: none;"></div>
                    <img class="pswp__img" src="https://farm2.staticflickr.com/1043/5186867718_06b2e9e551_b.jpg" style="display: block; width: 452px; height: 480px;">
                </div>
            </div>
            <div class="pswp__item" style="display: block; transform: translate3d(806px, 0px, 0px);">
                <div class="pswp__zoom-wrap" style="transform: translate3d(0px, 44px, 0px) scale(1);">
                <div class="pswp__img pswp__img--placeholder pswp__img--placeholder--blank" style="width: 720px; height: 480px; display: none;">
                </div>
                <img class="pswp__img" src="https://farm7.staticflickr.com/6175/6176698785_7dee72237e_b.jpg" style="width: 720px; height: 480px;">
                </div>
            </div>
        </div>
        <div class="pswp__ui pswp__ui--fit pswp__ui--hidden">
            <div class="pswp__top-bar">
                <div class="pswp__counter"></div>
                <img class="pswp__button pswp__button--close" style="margin-top:10px;margin-right:15px;height:27px;width:30px;" src="img/cancel.png" title="Close (Esc)"/>
                <img class="pswp__button pswp__button--fs" style="margin-top:10px;margin-right:15px;height:27px;width:30px;" src="img/full-screen.png"title="Toggle fullscreen" />
                <img class="pswp__button pswp__button--zoom" style="margin-top:10px;margin-right:15px;height:27px;width:30px;" src="img/search.png"title="Zoom in/out" />
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                      <div class="pswp__preloader__cut">
                        <div class="pswp__preloader__donut"></div>
                      </div>
                    </div>
                </div>
            </div>
            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div> 
            </div>
            <img style="margin-left:15px;height:40px;width:40px;" src="img/left-arrow.png" class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)" />
            <img style="margin-right:15px;height:40px;width:40px;" src="img/right-arrow.png" class="pswp__button pswp__button--arrow--right" title="Next (arrow right)" />
            <div class="pswp__caption"><div class="pswp__caption__center"></div></div>
        </div>
    </div>
</div>

@endsection