#!/usr/bin/php
<?php

$file = fopen( 'php://stdin', 'r');
$color=fgets($file);
$arrayColor=array();
do
{
	array_push($arrayColor,$color);
	$color=fgets($file);
}while($color!="\n");
$arrayRepresenteColor=array();//array represente color : -1 for black and 1 for white
for($i=0;$i<count($arrayColor);$i++)
{
	if((strcmp($arrayColor[$i],"black\n")==0) || (strcmp($arrayColor[$i],"Black\n")==0) )
	{
		array_push($arrayRepresenteColor,-1);
	}
	else
	{
		array_push($arrayRepresenteColor,1);
	}
}
$arrayResultat=array();
for($i=0;$i<count($arrayRepresenteColor);$i++)
{
	$often=1;
	for($j=$i+1;$j<count($arrayRepresenteColor);$j++)
	{
		$often*=$arrayRepresenteColor[$j];//multiple all elements  front of current player
	}
	if($i==0)
	{
		array_push($arrayResultat,$often);
	}
	else
	{
		$resultat1=1;
		for($k=0;$k<$i;$k++)
		{
			$resultat1*=$arrayResultat[$k];//multiple result of previous result
		}
		array_push($arrayResultat,$resultat1*$often);
	}
}
$mistake=0;
for($i=0;$i<count($arrayResultat);$i++)
{
	if($arrayResultat[$i]==1)
		echo "White";
	else
		echo "Black";
	if($arrayResultat[$i]==$arrayRepresenteColor[$i])
	{
		echo "    true guess"."\n";
	}
	else
	{
		echo "    false guess"."\n";	
		$mistake++;	
	}
}
if($mistake>1)
{
	echo "Game Over"."\n";
}
else
{
	echo "Win"."\n";
}
fclose($file);

?>