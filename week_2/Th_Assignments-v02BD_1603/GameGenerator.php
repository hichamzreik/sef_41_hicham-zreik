<?php

class GameGenerator{
	private $listGame;
	private $generateNumber;

	function __construct($list1,$list2)
	{
		shuffle($list1);
		$filtreList1=array_slice($list1, 0,rand(1,4));	
		shuffle($list2);
		$filtreList2=array_slice($list2,0,6-count($filtreList1));
		$this->listGame=array_merge($filtreList1,$filtreList2);
		$this->generateNumber=rand(100,999);
	}

	function getListGame()
	{
		return ($this->listGame);
	}

	function getGenerateNumber()
	{
		return ($this->generateNumber);
	}
	
}

?>