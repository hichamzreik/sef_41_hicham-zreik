<?php

require_once "GameGenerator.php";
require_once "GameSolver.php";

function pc_permute($items, $perms = array( )) 
{
	if (empty($items))
	{
	    $return = array($perms);
	}  
	else 
	{
	    $return = array();
	    for ($i = count($items) - 1; $i >= 0; --$i) 
	    {
	        $newitems = $items;
	        $newperms = $perms;
	        list($foo) = array_splice($newitems, $i, 1);
	        array_unshift($newperms, $foo);
	        $return = array_merge($return, pc_permute($newitems, $newperms));
	    }
	}
	return $return;
}

function getCombinations($base,$n)
{
	$baselen = count($base);
	if($baselen == 0)
	{
		return;
	}
	if($n == 1)
	{
		$return = array();
		foreach($base as $b)
		{
		    $return[] = array($b);
		}
		return $return;
	}
	else
	{
		//get one level lower combinations
		$oneLevelLower = getCombinations($base,$n-1);
		//for every one level lower combinations add one element to them that the last element of a combination is preceeded by the element which follows it in base array if there is none, does not add
		$newCombs = array();
		foreach($oneLevelLower as $oll)
		{
		    $lastEl = $oll[$n-2];
		    $found = false;
		    foreach($base as  $key => $b)
		    {
		        if($b == $lastEl)
		        {
		            $found = true;
		            continue;
		            //last element found
		        }
		        if($found == true)
		        {
		            //add to combinations with last element
		            if($key < $baselen)
		            {
		                $tmp = $oll;
		                $newCombination = array_slice($tmp,0);
		                $newCombination[]=$b;
		                $newCombs[] = array_slice($newCombination,0);
		            }

		        }
		    }

		}

	}

		    return $newCombs;
}

$list1=array(25, 50, 75, 100);
$list2=array(1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10);
$nbGame=readline("How many games would you like me to play today?");
for($j=0;$j<$nbGame;$j++)
{
	$gameGenerator=new GameGenerator($list1,$list2);
	$variableArray=array();
	$resultatPermutation= pc_permute($gameGenerator->getListGame(),$variableArray);
	$resultatCombinaison=array();
	foreach ($resultatPermutation as $value)
	{
	 	for($k = 2; $k<=6 ;$k++)
	 	{
	    	$comb = getCombinations($value,$k);
	     	foreach($comb as $c)
	     	{
	     		array_push($resultatCombinaison,$c);
	     	}
		}
	}
	$gameSolver=new GameSolver($resultatCombinaison,$gameGenerator->getGenerateNumber());
	$v=$gameSolver->solveGame();
	if($v!=0)
	{
		echo "Game : ".($j+1)."\n";
		echo "{";
		for($i=0;$i<6;$i++)
		{
			if($i<5)
			{
				echo $gameGenerator->getListGame()[$i].",";
			}
			if($i==5)
			{
				echo $gameGenerator->getListGame()[$i];
			}
		}
		echo "}.\n";
		echo "Target : ".$gameSolver->getGenerateNumb()."\n\n";
		if(($gameSolver->getGenerateNumb() - $v)==0)
		{
			echo "Solution[Exact]:\n";
		}
		else
		{	
			if($gameSolver->getGenerateNumb() >$v)
			{	
				echo "[Remaining : +".($gameSolver->getGenerateNumb()-$v)."]:\n";
			}
			else
			{	
				echo "[Remaining : -".($v-$gameSolver->getGenerateNumb())."]:\n";
			}
		}
		while(!$gameSolver->getStack()->isEmpty())
		{
			echo $gameSolver->getStack()->pop();
		}
		echo "=".$v;
		echo "\n";		
	}
	echo "\n";
}

?>