<?php

require_once "Stack.php";

class GameSolver{

	private $arrayGame;
	private $generateNumb;
	private $stack;

	function __construct($arrayGame,$generateNumb)
	{
		$this->generateNumb=$generateNumb;
		$this->arrayGame=$arrayGame;
		$this->stack = new Stack();
	}

	function getArrayGame()
	{
		return ($this->arrayGame);
	}

	function getGenerateNumb()
	{
		return ($this->generateNumb);
	}

	function getStack()
	{
		return ($this->stack);
	}

	function solveGame()
	{
		$x=0;
		$z=$this->generateNumb;
		$stack=$this->stack;
		foreach($this->arrayGame as $v)
		{
			if(count($v)>=2)
			{
				$q=$v[0]*$v[1];
				if( ($q==$z) || (($z-$q)>0 && ($z-$q)<=100) || (($q-$z)>0 && ($q-$z)<=100))
				{
					$x=$q;
					$stack->push(')');
					$stack->push($v[1]);
					$stack->push('*');
					$stack->push($v[0]);
					$stack->push('(');
					break ;
				}		
			}
			if(count($v)>=3)
			{
				$q=$v[0]+$v[1]+$v[2];
				if( ($q==$z) || (($z-$q)>0 && ($z-$q)<=100) || (($q-$z)>0 && ($q-$z)<=100))
				{
					$x=$q;
					$stack->push($v[2]);
					$stack->push('+');
					$stack->push($v[1]);
					$stack->push('+');
					$stack->push($v[0]);
					break ;
				}
				
				$q=($v[0]*$v[1])+$v[2];
				if( ($q==$z) || (($z-$q)>0 && ($z-$q)<=100) || (($q-$z)>0 && ($q-$z)<=100))
				{
					$x=$q;
					$stack->push($v[2]);
					$stack->push('+');
					$stack->push(')');
					$stack->push($v[1]);
					$stack->push('*');
					$stack->push($v[0]);
					$stack->push('(');
					break ;
				}
				
				$q=($v[0]*$v[2])+$v[1];
				if( ($q==$z) || (($z-$q)>0 && ($z-$q)<=100) || (($q-$z)>0 && ($q-$z)<=100))
				{
					$x=$q;
					$stack->push($v[2]);
					$stack->push('+');
					$stack->push(')');
					$stack->push($v[1]);
					$stack->push('*');
					$stack->push($v[0]);
					$stack->push('(');
					break ;
				}
				
				$q=($v[1]*$v[2])+$v[0];
				if( ($q==$z) || (($z-$q)>0 && ($z-$q)<=100) || (($q-$z)>0 && ($q-$z)<=100))
				{
					$x=$q;
					$stack->push($v[0]);
					$stack->push('+');
					$stack->push(')');
					$stack->push($v[2]);
					$stack->push('*');
					$stack->push($v[1]);
					$stack->push('(');
					break ;
				}
				
			}
			if(count($v)>=4)
			{
				$q=($v[0]*$v[1])+($v[2]*$v[3]);
				if( ($q==$z) || (($z-$q)>0 && ($z-$q)<=100) || (($q-$z)>0 && ($q-$z)<=100))
				{
					$x=$q;
					$stack->push(')');
					$stack->push($v[3]);
					$stack->push('*');
					$stack->push($v[2]);
					$stack->push('(');
					$stack->push('+');
					$stack->push(')');
					$stack->push($v[1]);
					$stack->push('*');
					$stack->push($v[0]);
					$stack->push('(');
					break ;
				}		
				$q=($v[0]*$v[1])+($v[1]*$v[3]);
				if( ($q==$z) || (($z-$q)>0 && ($z-$q)<=100) || (($q-$z)>0 && ($q-$z)<=100))
				{
					$x=$q;
					$stack->push(')');
					$stack->push($v[3]);
					$stack->push('*');
					$stack->push($v[1]);
					$stack->push('(');
					$stack->push('+');
					$stack->push(')');
					$stack->push($v[1]);
					$stack->push('*');
					$stack->push($v[0]);
					$stack->push('(');
					break ;
				}		
				$q=($v[0]*$v[1])+($v[1]*$v[2]);
				if( ($q==$z) || (($z-$q)>0 && ($z-$q)<=100) || (($q-$z)>0 && ($q-$z)<=100))
				{
					$x=$q;
					$stack->push(')');
					$stack->push($v[2]);
					$stack->push('*');
					$stack->push($v[1]);
					$stack->push('(');
					$stack->push('+');
					$stack->push(')');
					$stack->push($v[1]);
					$stack->push('*');
					$stack->push($v[0]);
					$stack->push('(');
					break ;
				}
				
				$q=($v[0]*$v[2])+($v[1]*$v[3]);
				if( ($q==$z) || (($z-$q)>0 && ($z-$q)<=100) || (($q-$z)>0 && ($q-$z)<=100))
				{
					$x=$q;
					$stack->push(')');
					$stack->push($v[3]);
					$stack->push('*');
					$stack->push($v[1]);
					$stack->push('(');
					$stack->push('+');
					$stack->push(')');
					$stack->push($v[2]);
					$stack->push('*');
					$stack->push($v[0]);
					$stack->push('(');
					break ;
				}
				
				$q=($v[0]*$v[2])+($v[2]*$v[1]);
				if( ($q==$z) || (($z-$q)>0 && ($z-$q)<=100) || (($q-$z)>0 && ($q-$z)<=100))
				{
					$x=$q;
					$stack->push(')');
					$stack->push($v[1]);
					$stack->push('*');
					$stack->push($v[2]);
					$stack->push('(');
					$stack->push('+');
					$stack->push(')');
					$stack->push($v[2]);
					$stack->push('*');
					$stack->push($v[0]);
					$stack->push('(');
					break ;
				}
				
				$q=($v[0]*$v[2])+($v[2]*$v[3]);
				if( ($q==$z) || (($z-$q)>0 && ($z-$q)<=100) || (($q-$z)>0 && ($q-$z)<=100))
				{
					$x=$q;
					$stack->push(')');
					$stack->push($v[3]);
					$stack->push('*');
					$stack->push($v[2]);
					$stack->push('(');
					$stack->push('+');
					$stack->push(')');
					$stack->push($v[2]);
					$stack->push('*');
					$stack->push($v[0]);
					$stack->push('(');
					break ;
				}
				
				$q=($v[0]*$v[3])+($v[3]*$v[1]);
				if( ($q==$z) || (($z-$q)>0 && ($z-$q)<=100) || (($q-$z)>0 && ($q-$z)<=100))
				{
					$x=$q;
					$stack->push(')');
					$stack->push($v[1]);
					$stack->push('*');
					$stack->push($v[3]);
					$stack->push('(');
					$stack->push('+');
					$stack->push(')');
					$stack->push($v[3]);
					$stack->push('*');
					$stack->push($v[0]);
					$stack->push('(');
					break ;
				}		
				$q=($v[0]*$v[3])+($v[3]*$v[2]);
				if( ($q==$z) || (($z-$q)>0 && ($z-$q)<=100) || (($q-$z)>0 && ($q-$z)<=100))
				{
					$x=$q;
					$stack->push(')');
					$stack->push($v[2]);
					$stack->push('*');
					$stack->push($v[3]);
					$stack->push('(');
					$stack->push('+');
					$stack->push(')');
					$stack->push($v[3]);
					$stack->push('*');
					$stack->push($v[0]);
					$stack->push('(');
					break ;
				}
				
				$q=($v[0]*$v[3])+($v[1]*$v[2]);
				if( ($q==$z) || (($z-$q)>0 && ($z-$q)<=100) || (($q-$z)>0 && ($q-$z)<=100))
				{
					$x=$q;
					$stack->push(')');
					$stack->push($v[2]);
					$stack->push('*');
					$stack->push($v[1]);
					$stack->push('(');
					$stack->push('+');
					$stack->push(')');
					$stack->push($v[3]);
					$stack->push('*');
					$stack->push($v[0]);
					$stack->push('(');
					break ;
				}		
			}
			if(count($v)==5)
			{
				$q=(($v[0]*$v[1])+($v[2]*$v[3]))+$v[4];
				if( ($q==$z) || (($z-$q)>0 && ($z-$q)<=100) || (($q-$z)>0 && ($q-$z)<=100))
				{
					$x=$q;
					$stack->push($v[4]);
					$stack->push('+');
					$stack->push(')');
					$stack->push($v[3]);
					$stack->push('*');
					$stack->push($v[2]);
					$stack->push('(');
					$stack->push('+');
					$stack->push(')');
					$stack->push($v[1]);
					$stack->push('*');
					$stack->push($v[0]);
					$stack->push('(');
					break ;
				}		
				$q=$v[0]+$v[1]+$v[2]+$v[3]+$v[4];
				if( ($q==$z) || (($z-$q)>0 && ($z-$q)<=100) || (($q-$z)>0 && ($q-$z)<=100))
				{
					$x=$q;
					$stack->push($v[4]);
					$stack->push('+');
					$stack->push($v[3]);
					$stack->push('+');
					$stack->push($v[2]);
					$stack->push('+');
					$stack->push($v[1]);
					$stack->push('+');
					$stack->push($v[0]);
				}				
			}
			if(count($v)==6)
			{
				$q=($v[2]*$v[3])+($v[4]*$v[5]);
				if( ($q==$z) || (($z-$q)>0 && ($z-$q)<=100) || (($q-$z)>0 && ($q-$z)<=100))
				{
					$stack->push(')');
					$stack->push($v[5]);
					$stack->push('*');
					$stack->push($v[4]);
					$stack->push('(');
					$stack->push('+');
					$stack->push(')');
					$stack->push($v[3]);
					$stack->push('*');
					$stack->push($v[2]);
					$stack->push('(');
					$x=$q;
					break ;
				}		
				$q=($v[0]*$v[1])+($v[4]*$v[5]);
				if( ($q==$z) || (($z-$q)>0 && ($z-$q)<=100) || (($q-$z)>0 && ($q-$z)<=100))
				{
					$stack->push(')');
					$stack->push($v[5]);
					$stack->push('*');
					$stack->push($v[4]);
					$stack->push('(');
					$stack->push('+');
					$stack->push(')');
					$stack->push($v[3]);
					$stack->push('*');
					$stack->push($v[2]);
					$stack->push('(');
					$x=$q;
					break ;
				}
				
			}
		}
		return $x;
	}
}

?>