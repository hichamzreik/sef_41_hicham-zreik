
function showMyImage(fileInput) 
{
   var files = fileInput.files;
   for (var i = 0; i < files.length; i++)
   {           
       var file = files[i];
       var imageType = /image.*/;     
       if (!file.type.match(imageType)) {
           Alert("Upload image only !");
           continue;
       }           
       var img=document.getElementById("imageToUpload");  
       img.style = "";          
       img.file = file;    
       var reader = new FileReader();
       reader.onload = (function(aImg) { 
           return function(e) { 
               aImg.src = e.target.result; 
           }; 
       })(img);
       reader.readAsDataURL(file);
   }    
}

$(document).ready(function() {
  $('#imageToUpload').click(function(){ $('#buttonUpload').trigger('click'); });

$(".btn-comment").click(function (event) {
      var url = "addComment";
      var postId = event.target.id;
      console.log("Post id: " + postId);
       var commentText = $(event.target).siblings("input").val();
      console.log(commentText);
      var name = $(event.target).data('name');
      console.log(name);
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      })
      var dataToSend = {
          'comment': commentText,
          'id': postId,
          'name': name,
      }
      $.ajax({
             type:'POST',
             url:'addComment',
             contentType: "json",
             processData: false,
             data:JSON.stringify(dataToSend),
             success:function(data){
                console.log(data);

             }, error:function(error) {
                  console.log(error);
             },
          });

  });

$(document).on('click', '.icon-0', function () {
            $(this).removeClass('icon-0').addClass('icon-1');
            var postID = $(this).data('post-id');
            $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
            })
            var dataToSend = {
                'reaction_type': "like",
                'post_id': postID,
            }
            $.ajax({
               type:'POST',
               url:'likeDislike',
               contentType: "json",
                  processData: false,
               data:JSON.stringify(dataToSend),
               success:function(data){
                  console.log(data);
               }, error:function(error) {
                    console.log(error);
               },
            });
        });

$(document).on('click','.icon-1',function () {
            $(this).removeClass('icon-1').addClass('icon-0');
            var postID = $(this).data('post-id');
            $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
            })
            var dataToSend = {
                'reaction_type': "dislike",
                'post_id': postID,
            }
            $.ajax({
               type:'POST',
               url:'likeDislike',
               contentType: "json",
                  processData: false,
               data:JSON.stringify(dataToSend),
               success:function(data){
                  console.log(data);
               }, error:function(error) {
                    console.log(error);
               },
            });
        });

});
