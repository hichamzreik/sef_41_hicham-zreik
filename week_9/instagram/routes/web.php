<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('/inst/home');
});

Auth::routes();

Route::get('/inst/home', 'ShowPostsController@index');

Route::post('/inst/addComment','ShowPostsController@addComment');

Route::post('/inst/likeDislike','ShowPostsController@handleReactionButton');

Route::get('/addPost','AddPostController@goToAddPost');

Route::post('/addPost','AddPostController@addPost');



?>