@extends('layouts.app')
@section('content')
<div class="container">
@foreach($posts as $post)
    <div class="row">
        <div class="col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading"><h1>{{$post->user->name}}</h1> </div>
                <div class="panel-body">
                    <div>
                         <img class="img-responsive center-block" src="{!! url('/imgUsers/' . $post->image_url) !!}">
                     </div>

                    <h3>{{ $post->post_text }}</h3>
                    <strong>Likers : </strong>{{$post->number_of_likes}}
                    <br/>
                    @if (count($post->comments))
                        @foreach($post->comments as $comment)
                            <div class=""> <!-- well well-sm -->
                                <strong>{{$comment->user->name}} </strong>: 
                                {{ $comment->comment }}
                            </div>
                        @endforeach
                    @else 
                            No comments
                    @endif  
                </div>
                <div class="panel-footer">
                     @if ($variable = 0) @endif
                    @foreach($post->likers as $like)
                        @if($like->user_id == Auth::id() && $like->post_id == $post->id)
                            @if ($variable = 1) @endif
                        @endif
                    @endforeach
                     <div class="reaction-button icon icon-{{$variable}} col-md-2" data-post-id ="{{ $post->id }}"></div>
                    <input id="comment" class="col-md-8 input-comment" name="comment" placeholder="comment..."></input>
                    <button class="btn btn-default btn-sm btn-comment" data-name="{{Auth::user()->name}}" id="{{ $post->id }}">comment</button>
                </div>
            </div>
        </div>
    </div>
@endforeach
</div>
@endsection