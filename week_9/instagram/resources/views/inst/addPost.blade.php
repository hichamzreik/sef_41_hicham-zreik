@extends('layouts.app')

@section('content')

<div class="container">
   <div class="row">
       <div class="col-md-8 col-md-offset-2">
           <div class="panel panel-default">
               <div class="panel-heading">New Post</div>
               <div class="panel-body">
                   <form role="form" action="{{ url('/addPost') }}" method="POST" enctype="multipart/form-data">
                           {{ csrf_field() }}
                       <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                       <img id="imageToUpload" style="" class="img-responsive image-uploaded" alt="Uploaded image here" required="true">
                        <br />
                       <label class="btn btn-default btn-file" style="visibility: hidden;" >
                           Browse <input id="buttonUpload" name="image" type="file" style="display: none;" accept="image/*" onchange="showMyImage(this)">
                       </label>
                       <br />
                       <br />
                       <label for="email" class="col-md-4 control-label">Description</label>
                       <textarea id="description" class="form-control" name="description" rows="5"></textarea><br/>
                       <button type="submit" class="btn btn-primary col-md-2 col-md-offset-10">Post</button>
                   </form> 
               </div>
           </div>
       </div>
   </div>
</div>
@endsection