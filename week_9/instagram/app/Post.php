<?php

namespace instagram;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
   protected $table = "posts";

   public function user() 
   {
        return $this->belongsTo('instagram\User');
   }

   public function comments() {
       return $this->hasMany('instagram\Comment');
   }

    public function likers() {
       return $this->hasMany('instagram\Liker');
   }

}

?>