<?php

namespace instagram;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
   protected $table = "comments";

   public function post() {
       return $this->belongsTo('instagram\Post');
   }
   
   public function user() 
   {
        return $this->belongsTo('instagram\User');
   }

}
