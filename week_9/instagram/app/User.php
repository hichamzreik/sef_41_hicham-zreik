<?php

namespace instagram;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use instagram\Comment;

class User extends Authenticatable
{
    use Notifiable;

    public function comments() {
       return $this->hasMany('instagram\Comment');
   }

   public function likers() {
       return $this->hasMany('instagram\Liker');
   }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','username'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
