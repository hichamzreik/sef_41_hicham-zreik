<?php

namespace instagram;

use Illuminate\Database\Eloquent\Model;

class Liker extends Model
{
    public function user() 
   {
        return $this->belongsTo('instagram\User');
   }

   public function post() 
   {
        return $this->belongsTo('instagram\Post');
   }
}
