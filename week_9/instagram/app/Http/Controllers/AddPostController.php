<?php

namespace instagram\Http\Controllers;

use Illuminate\Http\Request;

use instagram\Http\Requests;

use instagram\Post;

class AddPostController extends Controller
{
    public function goToAddPost()
    {
      return view('inst.addPost');
    }

    public function addPost(Request $request)
    {
      $this->validate($request, [

         'image' => 'required|image|mimes:jpeg,png,jpg',

      ]);

      $image = $request->file('image');
      $input['imagename'] = time().'_'.$request->user()->id.'_'.$image->getClientOriginalExtension();
      $destinationPath = public_path('imgUsers');
      $image->move($destinationPath, $input['imagename']);

      $newPost = new Post();
      $newPost->image_url = $input['imagename'];
      $newPost->post_text = $request->get('description');
      $newPost->user_id = $request->user()->id;
      $newPost->save();
      return redirect('/inst/home');
    }
}