<?php

namespace instagram\Http\Controllers;

use Illuminate\Http\Request;

use instagram\Http\Requests;

use Auth;

use instagram\Post;

use instagram\Comment;

use instagram\Liker;

use Illuminate\Support\Facades\DB;

class ShowPostsController extends Controller
{
    public function index()
    {
        
        //$posts =  Post::orderBy('created_at', 'DESC')->with('comments')->get();
        $posts =  Post::orderBy('created_at', 'DESC')->with('comments')->with('likers')->get();
       return view('inst.home') -> with('posts', $posts);
    }

    public function addComment(Request $request)
    {
		$data = json_decode($request->getContent(),true);
        $newComment = new Comment();
        $newComment->comment = $data['comment'];
        $newComment->post_id = $data['id'];
        $newComment->user_id = Auth::id();
        $newComment->save();
        return response()->json(array('comment'=> $data), 200);
    }

    public function handleReactionButton(Request $request) {
        $data = json_decode($request->getContent(),true);
        $postId = $data['post_id'];
        $reactionType = $data['reaction_type'];
        if($reactionType === "like") {
            DB::table('posts')
                ->whereId($postId)->increment('number_of_likes');
            $newLike = new Liker();
            $newLike->user_id = Auth::id();
            $newLike->post_id = $postId;
            $newLike->save();
        }
        else if($reactionType === "dislike") {
            DB::table('posts')
            ->whereId($postId)->decrement('number_of_likes');
            $like = Liker::where('user_id', Auth::id())->where('post_id', $postId)->first();
            $like->delete();
        }

         return response()->json(array('like-dislike'=> "success"), 200);
    }
}
