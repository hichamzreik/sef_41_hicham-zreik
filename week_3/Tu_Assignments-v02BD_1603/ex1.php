<?php

require_once "DataBase.php";

$input = fopen( 'php://stdin', 'r');

do
{
	$query=fgets($input);
	//array contain word of query
	$arrayQuery=explode(",",$query);
	if ((strcmp($arrayQuery[0],"CREATE") == 0) && (strcmp($arrayQuery[1],"DATABASE") == 0)) {
		if (count($arrayQuery) == 3) {
			//elimine guillemets from nameDataBase
			$arrayNameDataBase=explode("\"",$arrayQuery[2]);
			$nameDataBase=$arrayNameDataBase[1];
			if ( !file_exists($nameDataBase)) {	
				$dataBase=new DataBase();
				$dataBase->setNameDataBase($nameDataBase);			
				$dataBase->createDataBase();
			} else {
				echo "This database exist.\n";
			}			
		} else {
			echo "error query! \n";
		}
	} elseif ((strcmp($arrayQuery[0],"DELETE") == 0) && (strcmp($arrayQuery[1],"DATABASE") == 0)) {
		if (count($arrayQuery) == 3) {
			$nameDirectory=explode("\"",$arrayQuery[2]);
			$dataBase->deleteDataBase($nameDirectory[1]);	
		} else {
			echo "error query ! \n";
		}
	} elseif ((strcmp($arrayQuery[0],"CREATE") == 0) && (strcmp($arrayQuery[1],"TABLE") == 0)) {
		if (count($arrayQuery) >= 5) {
			if (strcmp($arrayQuery[3],"COLUMNS") == 0) {
				//elimine guillemets from nameTable
				$arrayNameTable=explode("\"",$arrayQuery[2]);
				$nameTable=$arrayNameTable[1];
				if (!file_exists($nameTable.".txt")) {
					$dataBase->setNameTable($nameTable);
					$dataBase->createTable();
					for($i=4;$i<count($arrayQuery);$i++)
					{
						if ($i == count($arrayQuery)-1) {
							$dataBase->addColumn($arrayQuery[$i],'');
						} else {
							$dataBase->addColumn($arrayQuery[$i],',');
						}
					}
				} else {
					echo "This table exist \n";
				}
			} else {
				echo "error syntax query !\n";
			}
		} else {
			echo "error query!\n";
		}
	} elseif (strcmp($arrayQuery[0],"ADD") == 0) {
		//verifie number of attributes is equal to number of columns
		if ((count($arrayQuery)-1) == $dataBase->getNumbColumns()) {
			//to get the id
			$arrayId = explode("\"",$arrayQuery[1]);
			$firstColumn = $arrayId[1];
			//verifie if first column is integer
			if (is_numeric($firstColumn)) {
				if (!$dataBase->ifExist($firstColumn)) {
					for($i=1;$i<count($arrayQuery);$i++)
					{
						//print last element without ;
						if($i==count($arrayQuery)-1) {
							$dataBase->addRecord($arrayQuery[$i],'');
						} else {
							$dataBase->addRecord($arrayQuery[$i],',');
						}								
					}
					echo "Record ADDED\n";
				} else {
					echo "this id exist\n";
				} 
			} else {
				echo "First column should be an Integer\n";	
			}
		} else {
			echo "Pay attention of number of attribut in record!\n";
		}								
	}elseif (strcmp($arrayQuery[0],"GET") == 0) {
		if (count($arrayQuery) == 2) {
			$arrayValue=explode("\"",$arrayQuery[1]);
			$value=$arrayValue[1];
			$dataBase->getRecord($value);
		} else {
			echo "error query!\n";
		}
	} elseif (strcmp($arrayQuery[0],"DELETE") == 0 && strcmp($arrayQuery[1],"ROW") == 0) {
		if(count($arrayQuery) == 3) {
			$arrayId=explode("\"",$arrayQuery[2]);
			$id=$arrayId[1];
			if(is_numeric($id)){
				$dataBase->deleteRecord($id);
				echo "Record Delete\n";
			} else {
				echo "delete record should take an Integer.\n";
			}
		}
	} else {
		echo "error query ! \n";
	}

} while(true);

fclose($input);

?>