<?php

class DataBase{

	private $nameDataBase;
	private $nameTable;
	private $numbColumns=0;
	private $pathLocation;

	function __construct()
	{
		$this->nameDataBase="";
		$this->nameTable="";
		$this->pathLocation=getcwd();
	}

	function getPathLocation()
	{
		return ($this->path);
	}

	function getNameDataBase()
	{
		return ($this->nameDataBase);
	}

	function getNameTable()
	{
		return ($this->nameTable);
	}

	function getNumbColumns()
	{
		return ($this->numbColumns);
	}

	function setNameDataBase($nameDataBase)
	{
		$this->nameDataBase=$nameDataBase;
	}

	function setNameTable($nameTable)
	{
		$this->nameTable=$nameTable;
	}

	function createDataBase()
	{
		
		mkdir($this->nameDataBase,0777);
		echo "\"".$this->nameDataBase."\" "."CREATED\n";
	}

	function deleteDataBase($nameDirectory)
	{
		$pathDirectory=$this->pathLocation."/".$nameDirectory;
		shell_exec('rm -r '.$pathDirectory);
		echo "\"".$nameDirectory."\" "."DELETED\n";
		$this->nameDataBase="";
	}

	function createTable()
	{
		//initial numb of columns if we have created another table
		$this->numbColumns=0;
		//go to directory of DataBase if it isn't
		if(strcmp(basename(getcwd()),$this->nameDataBase)!=0) {
			chdir($this->nameDataBase);
		}
		$myTable=fopen($this->nameTable.".txt","w");
		fclose($myTable);
		echo "\"".$this->nameTable."\" "."CREATED\n";
	}

	function addColumn($nameColumn,$comma)
	{
		$myTable=fopen($this->nameTable.".txt","a");
		//elimine guillemets
		$nameColumn=str_replace('"','', $nameColumn);
		fwrite($myTable,$nameColumn.$comma);
		fclose($myTable);
		$this->numbColumns+=1;
	}

	function addRecord($attribute,$comma)
	{
		$myTable=fopen($this->nameTable.".txt","a");
		//elimine guillemets
		$attribute=str_replace('"','', $attribute);
		fwrite($myTable,$attribute.$comma);
		fclose($myTable);
	}

	function getRecord($value)
	{
		$table=fopen($this->nameTable.".txt", 'r');
		while(!feof($table))
		{
			$line=fgets($table);
			//array contain elements of line
			$arrayLine=explode(",",$line);
			if (is_numeric($value)) {
				if($arrayLine[0] == $value) {
					echo $line;
				}
			} else {
				for($i=1;$i<count($arrayLine);$i++)
				{
					if (strcmp($value,trim($arrayLine[$i])) == 0) {
						echo $line;
					}
				}
			}
		}		
		fclose($table);
	}

	function deleteRecord($id)
	{
		//$data contain all content file into array
		$data = file($this->nameTable.".txt");
		$out = array();
		foreach($data as $line)
		{
			$arrayLine=explode(",",trim($line));
	        if($arrayLine[0] != $id) {
	             $out[] = $line;
	        }
     	}
     	//reopen file 
     	$fp=fopen($this->nameTable.".txt", "w+");
     	//Prevent other processes from accessing the file
	    flock($fp, LOCK_EX);
	    foreach($out as $line) 
	    {
	        fwrite($fp, $line);
	    }
	    //Release a shared or exclusive lock
	    flock($fp, LOCK_UN);
	    fclose($fp);
	}

	function ifExist($id)
	{
		$table=fopen($this->nameTable.".txt", 'r');
		while(!feof($table))
		{
			$line=fgets($table);
			$arrayL=explode(",",$line);
			if($arrayL[0] == $id) {
				fclose($table);
				return 1;
			}
		}		
		fclose($table);
		return 0;
	}

}

?>