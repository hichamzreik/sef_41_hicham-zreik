<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/index','BlogController@index');

Route::group(['middleware' => ['auth']], function()
{
	Route::get('/post','MakePostController@addPost');
	Route::get('/index/{id}','DetailController@detail');
	Route::get('/addPost','MakePostController@goToPost');
	Route::post('/newPost','MakePostController@addPost');
	Route::get('/delete/{id}','BlogController@delete');
});

?>







