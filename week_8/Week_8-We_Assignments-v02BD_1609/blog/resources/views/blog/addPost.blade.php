@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">New Post</div>
                <div class="panel-body">
                    <form role="form" action="{{ url('/newPost') }}" method="POST" >
                            {{ csrf_field() }}
                        <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                        <label for="title" class="col-md-4 control-label">Title</label>
                        <input id="title" type="text" class="form-control" name="title"  required autofocus><br/>
                        <label for="email" class="col-md-4 control-label">Description</label>
                        <textarea class="form-control" name="description" rows="5"></textarea><br/>
                        <button type="submit" class="btn btn-primary col-md-2 col-md-offset-10">Post</button>
                    </form> 
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
