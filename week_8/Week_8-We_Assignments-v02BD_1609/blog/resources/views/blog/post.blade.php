@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading" style="word-wrap: break-word;"><a><h1>{{ $post->title }}</h1></a></div>
                <div class="panel-body">
                       <div class="row" style="word-wrap: break-word;">
                          {{ $post->description }}
                       </div>
                       @if(Auth::id() == $post->idUser)
                       <div class="col-md-offset-10"> <a href="{{ url('delete/'.$post->id) }}">Delete</a> </div>
                      @endif
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
