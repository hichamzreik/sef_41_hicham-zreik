@extends('layouts.app')

@section('content')

@foreach ($blogs as $blog)
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading" style="word-wrap: break-word;"><a><h1>{{ $blog->title }}</h1></a></div>
                <div class="panel-body">                   
                   <div class="row">                    
                     <h3>{{str_limit($blog->description,40)}}</h3>
                      <a href="{{ url('index/'.$blog->id) }}">Read More...</a>
                      @if(Auth::id() == $blog->idUser)
                       <div class="col-md-offset-10"> <a href="{{ url('delete/'.$blog->id) }}">Delete</a> </div>
                      @endif
                   </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach

<div class="col-md-offset-5">{{ $blogs->links() }}</div>

@endsection
