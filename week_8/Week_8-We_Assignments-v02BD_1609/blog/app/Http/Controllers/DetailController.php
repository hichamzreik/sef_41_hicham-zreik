<?php

namespace blog\Http\Controllers;

use Illuminate\Http\Request;

use blog\Http\Requests;

use blog\Blog;

class DetailController extends Controller
{
    public function detail($id)
    {
    	$posts = Blog::where('id',$id)->first();
        if(!$posts)
        {
           return redirect('/')->withErrors('requested page not found');
        }
        return view('blog.post')->with('post',$posts);
    }
}
