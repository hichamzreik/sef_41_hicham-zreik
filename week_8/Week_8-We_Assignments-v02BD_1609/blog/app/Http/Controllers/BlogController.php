<?php

namespace blog\Http\Controllers;

use Illuminate\Http\Request;

use blog\Blog;

class BlogController extends Controller
{
	
	public function index()
	{
		$blogs = Blog::paginate(5);
        return view('blog.index')->with('blogs',$blogs);
	}

	function delete($id)
	{
		$blog = Blog::find($id); 
		$blog->delete();
		return redirect('/index');
	}
}
