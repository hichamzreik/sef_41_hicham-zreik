<?php

namespace blog\Http\Controllers;

use Illuminate\Http\Request;

use blog\Http\Requests;

use Illuminate\Foundation\Validation\ValidatesRequests;

use blog\Blog;


class MakePostController extends Controller
{
    public function goToPost()
    {
    	return view('blog.addPost');
    }

    public function addPost(Request $request) 
    {
    	$newBlog = new Blog() ;
        $newBlog->title = $request->get('title');
        $newBlog->description = $request->get('description');
        $newBlog->idUser = $request->user()->id;
        $newBlog->save();
        return redirect('/index');
    }

}
