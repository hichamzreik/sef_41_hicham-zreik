
var arrayFrom = [];
var arrayTo = [];
var arrayDisk = [];
var i=0;

function start()
{
	document.getElementById("start").disabled="false";

	document.getElementById("disk1").style["left"] = '0px';
	document.getElementById("disk2").style["left"] = '0px';
	document.getElementById("disk3").style["left"] = '0px';
	document.getElementById("disk4").style["left"] = '0px';
	document.getElementById("disk5").style["left"] = '0px';
	document.getElementById("disk6").style["left"] = '0px';
	document.getElementById("disk7").style["left"] = '0px';
	document.getElementById("disk8").style["left"] = '0px';

	Hanoi(8,"A","C","B");

	for(var j=0 ; j < arrayFrom.length; j++)
	{
		// Creates a new <div> node
		var x = document.createElement("div");
		// Sets the text content
		x.textContent = "Move " + arrayDisk[j] + " from " + arrayFrom[j] + " to " + arrayTo[j]; 
		document.getElementById('moves').appendChild(x);
	}

	//this function doesn't in for because animation slower than loop in this case thread main pace thread animation
	move(arrayFrom[0], arrayTo[0], document.getElementById(arrayDisk[0]), 0);
}	

function Hanoi(n, from, to, via) 
{
		if (n == 0) return;
		Hanoi(n-1, from, via, to);

		//save steps hanoi in these arrays
		arrayFrom[i] =  from;
		arrayTo[i] = to;
		arrayDisk [i] = "disk"+n;
		i++;

		Hanoi(n-1, via, to, from);
}


function move( from, to, disk, j)
{
	if (from == "A" && to=="B") {
		direction = 1;
		leftProperty = 300 + 'px';
	} else if (from == "A" && to == "C") {
		direction = 1;
		leftProperty = 600 + 'px';
	} else if (from == "B" && to == "C") {
		direction = 1;
		leftProperty = 600 + 'px';
	} else if (from == "B" && to == "A") {
		direction = -1;
		leftProperty = 0 + 'px';
	} else if (from == "C" && to == "B" ){
		direction = -1;
		leftProperty = 300 + 'px';
	} else {
		direction = -1 ;
		leftProperty = 0 + 'px';
	}

	//setInterval Repeatedly calls a function with a fixed time delay between each call
	var moveIt = setInterval(function() {
	// parseInt to take number without pixel
	disk.style["left"] = parseInt(disk.style.left) + direction * 10 + 'px';
	diskLeft = parseInt(disk.style.left);
	goalLeft = parseInt(leftProperty);

	//to check if disk arrive where it should arrive
	canMove = true;
	if (direction == -1) {
		//this case when disk arrive to bar which should arrive
		if(diskLeft <= goalLeft) {
			canMove = false;
		}
	} else if (diskLeft >= goalLeft) {
			canMove = false;
	}
	if(!canMove) {
		//clearInterval Cancels repeated action
		clearInterval(moveIt);
		j++;
		if(j < arrayFrom.length) {
			//to move the another disk
			move(arrayFrom[j], arrayTo[j], document.getElementById(arrayDisk[j]), j);
		}
	}
	}, 20);

}

