<?php

require_once "MySQLWrap.php";
require_once "Config.php";
session_start();

//when the film isn't exist in the store
if ($_SESSION["try"] == 1) {
	echo "This film doesn't exist in this store , Try another store.<br/><br/>";
	$_SESSION["try"]=0;
}

//when return date greather than rental date
if ($_SESSION["errorDate"] == 1) {
	echo "Error in return date.<br/><br/>";
	$_SESSION["errorDate"] = 0;
}

//when insert successfully
if ($_SESSION["success"] == 1) {
	echo "Insert successfully.<br/><br/>";
	$_SESSION["success"] = 0;
}

//connect to database sakila
$mySqlWrap = new MySQLWrap($serverName, $userName, $passwordUser, $dbName);
$mySqlWrap->connect();

//get films
$resultFilm = $mySqlWrap->getFilm();
//get the number of rows
$rowsFilm = $resultFilm->num_rows;

//get stores
$resultStore = $mySqlWrap->getStore();
$rowsStore = $resultStore->num_rows;

?>

<html>
	<head>
	<!-- to make datepicker function in fireFox  -->
		<script type="text/javascript" src="http://code.jquery.com/jquery-2.1.4.min.js"></script> 
		<script src="//cdn.jsdelivr.net/webshim/1.14.5/polyfiller.js"></script>
		<script>
		webshims.setOptions('forms-ext', {types: 'date'});
		webshims.polyfill('forms forms-ext');
		$.webshims.formcfg = {
		en: {
		    dFormat: '-',
		    dateSigns: '-',
		    patterns: {
		        d: "yy-mm-dd"
		    }
		}
		};
		</script>
	</head>
	<body>
		<form action = "OrderProcess2.php" method = "post">
			<label for = "filme">Film : </label>
			<select name = "film">
				<?php for ($i = 0; $i < $rowsFilm; $i++)
						{
							//seek to row specifie by $i
							$resultFilm->data_seek($i);
							//fetch row
							$row = $resultFilm->fetch_array(MYSQLI_ASSOC);
				?>
				<option value = <?php echo $row['film_id'] ; ?> > <?php echo $row['title']; ?> </option>
				<?php } ?>	
			</select>
			<br/><br>
			<label for = "store">Store : </label>
			<select name = "store">
				<?php for ($i = 0; $i < $rowsStore; $i++)
						{
							//seek to row specifie by $i
							$resultStore->data_seek($i);
							//fetch row
							$row = $resultStore->fetch_array(MYSQLI_ASSOC);
				?>
				<option value = <?php echo $row['store_id'] ; ?> > <?php echo $row['store_id']; ?> </option>
				<?php } ?>	
			</select>
			<br/><br/>
			Return Date : <input type = "date" placeholder = "yyyy-mm-dd" name = "date" required = "true" />
			<br/><br/>
			Return Time : <input type = "text" name = "time" placeholder = "H:i:s" required = "true" />
			<br/><br/>
			<input type = "submit" name = "insert" value = "Insert"/>
		</form>
	</body>
</html>