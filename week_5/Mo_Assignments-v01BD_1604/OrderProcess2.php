<?php

require_once "MySQLWrap.php";
require_once "Config.php";
session_start();

//function to calculate the payment which should customer pay
function calculateRentalPayment($rental_date, $return_date, $rental_duration, $rental_rate) 
{
	//Returns new DateTime object
	$dt = new DateTime($rental_date);
	$rental_date = $dt->format('Y-m-d');
	$dt = new DateTime($return_date);
	$return_date = $dt->format('Y-m-d');

	// Parse about any English textual datetime description into a Unix timestamp
	$rental_date = strtotime($rental_date);
	$return_date = strtotime($return_date);
	$datediff = $return_date - $rental_date;

	//get number of days from the difference
	$days=floor($datediff/(60*60*24));
    $payment = ($days*$rental_rate)/$rental_duration;
    return  $payment;
}

$filmId = $_POST['film'];
$storeId = $_POST['store'];
$mySqlWrap = new MySQLWrap($serverName, $userName, $passwordUser, $dbName);
$mySqlWrap->connect();

$resultInventory = $mySqlWrap->getInventoryId($filmId, $storeId);
$rowsInventory = $resultInventory->num_rows;
//meaning the film isn't exist in the store
if ($rowsInventory == 0) {
	$_SESSION["try"] = 1;
	header("Location:Order2.php");
} else {
	//get the id of inventory
	$resultInventory->data_seek(0);
	$row = $resultInventory->fetch_array(MYSQLI_ASSOC);
	$inventoryId = $row['inventory_id'];

	//get the id of staff
	$resultStaff = $mySqlWrap->getStaffId($storeId);
	$resultStaff->data_seek(0);
	$row = $resultStaff->fetch_array(MYSQLI_ASSOC);
	$staffId = $row['staff_id'];

	//get date now with time
	date_default_timezone_set("Asia/Beirut");
	$rental_date = date("Y-m-d")." ".date("h:i:sa");
	$last_update = date("Y-m-d")." ".date("h:i:sa");

	//get date of retrun rental
	$date = $_POST['date'];
	$time = $_POST['time'];
	$return_date = $date." ".$time;
	if ($return_date < $rental_date ) {
		$_SESSION["errorDate"] = 1;
		header("Location:Order2.php");
	} else {
		//insert to rental table
		$successInsert = $mySqlWrap->insertRental($rental_date, $inventoryId,$return_date, $staffId, $last_update);
		if ($successInsert = 1) {
			//get last insert id from rental table
			$result = $mySqlWrap->getLastInsertId();
			$result->data_seek(0);
			$row = $result->fetch_array(MYSQLI_ASSOC);
			$rentalId = $row['rental_id'];

			//get rental duration and rental rate from film table
			$resultRental = $mySqlWrap->getAmount($filmId);
			$resultRental->data_seek(0);
			$row = $resultRental->fetch_array(MYSQLI_ASSOC);
			$rental_duration = $row['rental_duration'];
			$rental_rate = $row['rental_rate'];

			//calculate the payment
			$payment = calculateRentalPayment($rental_date , $return_date ,$rental_duration, $rental_rate); 
			echo $payment;

			//insert amount to table payment
			$successPayment = $mySqlWrap->Payment($staffId, $rentalId, $payment, $rental_date, $last_update);
			if($successPayment == 1) {
				$_SESSION["success"] = 1;
				header("Location:Order2.php");
			}
		}
	}
}

?>