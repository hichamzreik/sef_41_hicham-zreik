<?php
require_once 'config.php';

class Film {
	private $db = NULL;

	public function __construct() {
		$this->dbConnect();	// Initiate Database connection
	}

	private function dbConnect(){
	        $this->db = new mysqli(DB_SERVER,DB_USER,DB_PASSWORD,DB);

	        if ($this->db->connect_error) {
	             die ("could not connect");
	        }
	}

	public function all() {
		$myArray = Array();
	        $result = $this->db->query("select * from film");
	        while($row = $result->fetch_array(MYSQL_ASSOC)) {
	            $myArray[] = $row;
	        }
	    return $myArray;
	}

	public function getFilm($id) {
		$myArray = Array();
		if(is_numeric($id)){
			$result = $this->db->query("select * from film where film.film_id=$id");
	        while($row = $result->fetch_array(MYSQL_ASSOC)) {
	            $myArray[] = $row;
	        }
	    	return $myArray;
		}
		else {
			echo "select * from film where film.title=$id";
			$result = $this->db->query("select * from film where film.title='$id'");
	        while($row = $result->fetch_array(MYSQL_ASSOC)) {
	            $myArray[] = $row;
	        }
	    	return $myArray;
	        
		}
	}

	public function deleteFilm($id) {
		$myArray = Array();
		if(is_numeric($id)){
			$result = $this->db->query("delete from film where film.film_id='$id'");
			echo "delete from film where film.film_id=$id";
		}
		else {
			$result = $this->db->query("delete from film where film.title='$id'");       
		}
	}
}

?>
