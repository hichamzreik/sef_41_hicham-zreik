<?php

require_once 'Controllers/filmController.php';

class API {
    public $data = "";
    
    public $_content_type = "application/json";
    public $_request = array();
    
    protected $mySQL;
    /**
     * Property: method
     * The HTTP method this request was made in, either GET, POST, PUT or DELETE
     */
    protected $method = '';
    /**
     * Property: endpoint
     * The Model requested in the URI. eg: /files
     */
    protected $endpoint = '';
    /**
     * Property: verb
     * An optional additional descriptor about the endpoint, used for things that can
     * not be handled by the basic methods. eg: /files/process
     */
    protected $verbs = [];
    /**
     * Property: args
     * Any additional URI components after the endpoint and verb have been removed, in our
     * case, an integer ID for the resource. eg: /<endpoint>/<verb>/<arg0>/<arg1>
     * or /<endpoint>/<arg0>
     */
    protected $args = Array();
    /**
     * Property: file
     * Stores the input of the PUT request
     */
    protected $file = Null;
    protected $params ='';
    private $_code = 200;

    protected $filmController;
 
    public function __construct(){
        $this->inputs();         // Init parent contructor
        $this->filmController = new filmController();
    }

    public function get_referer() {
        return $_SERVER['HTTP_REFERER'];
    }

    public function response($data,$status){
        $this->_code = ($status)?$status:200;
        $this->set_headers();
        echo $data;
        exit;
    }

    private function inputs() {
        switch($this->get_request_method()) {
            case "POST":
                $this->_request = $this->cleanInputs($_POST);
                break;
            case "GET":
                $this->_request = $this->cleanInputs($_GET);
            case "DELETE":
                $this->_request = $this->cleanInputs($_GET);
                break;
            case "PUT":
                parse_str(file_get_contents("php://input"),$this->_request);
                $this->_request = $this->cleanInputs($this->_request);
                break;
            default:
                $this->response('',406);
                break;
        }
    }
     
    
         
        /*
         * Public method for access api.
         * This method dynmically call the method based on the query string
         *
         */
   public function processApi(){

        $this->args = explode('/', rtrim($_REQUEST['rquest'], '/'));
        $daE = $this->get_request_endpoint();
        // $daP = $this->get_request_params();
        $daV = $this->get_request_verbs();

        $getReq = Array();
        foreach($_GET as $key=>$value){
            // array_push($getReq, $value);
            $getReq[$key]=$value;
        }

        if($daE == "film") {
            if(!is_null($daV) && sizeof($daV) > 0){
                if($daV[0] == "all") {
                    $this->filmController->all();
                }
                if($daV[0] == "delete") {
                    if(isset($getReq['id'])) {
                        $this->filmController->deleteFilm($getReq['id']);
                    }
                }
            }
            else if(isset($getReq['id']) ) {
                $this->filmController->getFilm($getReq['id']);
            }
        }
    }
  
    /*
     *  Encode array into JSON
    */
    private function json($data){
        if(is_array($data)){
            return json_encode($data);
        }
    }

    private function get_status_message() {
        $status = array(
                    100 => 'Continue',  
                    101 => 'Switching Protocols',  
                    200 => 'OK',
                    201 => 'Created',  
                    202 => 'Accepted',  
                    203 => 'Non-Authoritative Information',  
                    204 => 'No Content',  
                    205 => 'Reset Content',  
                    206 => 'Partial Content',  
                    300 => 'Multiple Choices',  
                    301 => 'Moved Permanently',  
                    302 => 'Found',  
                    303 => 'See Other',  
                    304 => 'Not Modified',  
                    305 => 'Use Proxy',  
                    306 => '(Unused)',  
                    307 => 'Temporary Redirect',  
                    400 => 'Bad Request',  
                    401 => 'Unauthorized',  
                    402 => 'Payment Required',  
                    403 => 'Forbidden',  
                    404 => 'Not Found',  
                    405 => 'Method Not Allowed',  
                    406 => 'Not Acceptable',  
                    407 => 'Proxy Authentication Required',  
                    408 => 'Request Timeout',  
                    409 => 'Conflict',  
                    410 => 'Gone',  
                    411 => 'Length Required',  
                    412 => 'Precondition Failed',  
                    413 => 'Request Entity Too Large',  
                    414 => 'Request-URI Too Long',  
                    415 => 'Unsupported Media Type',  
                    416 => 'Requested Range Not Satisfiable',  
                    417 => 'Expectation Failed',  
                    500 => 'Internal Server Error',  
                    501 => 'Not Implemented',  
                    502 => 'Bad Gateway',  
                    503 => 'Service Unavailable',  
                    504 => 'Gateway Timeout',  
                    505 => 'HTTP Version Not Supported');
        return ($status[$this->_code])?$status[$this->_code]:$status[500];
    }

    public function get_request_method() {
        return $_SERVER['REQUEST_METHOD'];
    }

    public function get_request_endpoint() {
        return $this->endpoint = array_shift($this->args);
    }

    public function get_request_verbs() {
        while (array_key_exists(0, $this->args) && !is_numeric($this->args[0])) {
            $this->verbs[] = array_shift($this->args);
        }

        return $this->verbs;
    }

    // public function get_request_params() {
    //     if (array_key_exists(0, $this->args) && is_numeric($this->args[0])) {
    //         return $this->params = array_shift($this->args);
    //     }
    // }

    private function cleanInputs($data) {
        $clean_input = array();
        if(is_array($data)){
            foreach($data as $k => $v){
                $clean_input[$k] = $this->cleanInputs($v);
            }
        }else{
            if(get_magic_quotes_gpc()){
                $data = trim(stripslashes($data));
            }
            $data = strip_tags($data);
            $clean_input = trim($data);
        }
        return $clean_input;
    }       
     
    private function set_headers() {
        header("HTTP/1.1 ".$this->_code." ".$this->get_status_message());
        header("Content-Type:".$this->_content_type);
    }

}

    $api = new API;
    $api->processApi();
?>
