<?php
require_once('Models/Film.php');

class filmController {

	protected $film;

	public function __construct() {
		$this->film = new Film();
	}

	public function all() {
		print_r($this->film->all());
	}

	public function getFilm($id) {
		print_r($this->film->getFilm($id));
	}

	public function deleteFilm($id) {
		print_r($this->film->deleteFilm($id));
	}

}

?>
