<?php
    
     // $homepage = file_get_contents('https://medium.freecodecamp.com/the-art-of-computer-programming-by-donald-knuth-82e275c8764f#.5gqjhn5al');
     //       echo $homepage;

?>

<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Summarizer</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="css/normalize.min.css">
        <link rel="stylesheet" href="css/main.css">

        <script src="http://code.jquery.com/jquery-1.5.2.js"></script>
        <script src="js/main.js"></script>
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>

        <input type="text" name="text-url" id="text_url">
        <button id="button">Go</button>

        <form name="myform" action="<?php echo $_SERVER['$PHP_SELF']; ?>" method="POST">
            <input type="hidden" name="hidden1" id="hidden1"  />
        </form>

    </body>
</html>
