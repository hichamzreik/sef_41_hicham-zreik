
$(document).ready(function(){
$("#button").click(function(){
  var url = document.getElementById("text_url").value;
  doAjax(url);
});
// doAjax('http://medium.freecodecamp.com/the-art-of-computer-programming-by-donald-knuth-82e275c8764f#.undnextd0');
function doAjax(url){
  // if it is an external URI
  if(url.match('^http')){
    // call YQL
    $.getJSON("http://query.yahooapis.com/v1/public/yql?"+
              "q=select%20*%20from%20html%20where%20url%3D%22"+
              encodeURIComponent(url)+
              "%22&format=xml'&callback=?",
      // this function gets the data from the successful 
      // JSON-P call
      function(data){
        // if there is data, filter it and render it out
        if(data.results[0]){
          var data = filterData(data.results[0]);
         // container.html(JSON.stringify(data));
          console.log("sucess");
         // console.log(data);
          getAnchorTexts(data);
        // otherwise tell the world that something went wrong
        } else {
          var errormsg = '<p>Error: cant load the page.</p>';
        //  container.html(errormsg);
          console.log("error");
        }
      }
    );
  // if it is not an external URI, use Ajax load()
  } else {
    $('#target').load(url);
  }
}
// filter out some nasties
function filterData(data){
  data = data.replace(/<?\/body[^>]*>/g,'');
  data = data.replace(/[\r|\n]+/g,'');
  data = data.replace(/<--[\S\s]*?-->/g,'');
  data = data.replace(/<noscript[^>]*>[\S\s]*?<\/noscript>/g,'');
  data = data.replace(/<script[^>]*>[\S\s]*?<\/script>/g,'');
  data = data.replace(/<script.*\/>/,'');
  return data;
}

function getAnchorTexts(htmlStr) {
  var div,
      paragraphs,
      texts;
  div = document.createElement('div');
  div.innerHTML = htmlStr;
  var outerDiv = div.getElementsByClassName("section-content");
  outerDiv = outerDiv[0];
  var innerDiv = outerDiv.firstChild;
  paragraphs = innerDiv.getElementsByTagName('p');
  console.log(paragraphs);
  //texts = [];
  theText = '';
  for (var i = 0; i < paragraphs.length; i += 1) {
    var  text = paragraphs[i].innerText;
    if(typeof text == "string")
       // texts.push(text);
     theText += text;
  }
  // return texts;
  console.log("***************");
  console.log(theText);
}
});