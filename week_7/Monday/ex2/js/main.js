
window.onload = function() {
 displayItems();
};

function add()
{
	//localStorage.clear();
	if( document.getElementById('remember').value.trim() == "" || document.getElementById('textR').value.trim() == "" ) {
		alert("Fill all input");
	} else {
		var currentdate = new Date(); 
		var datetime = currentdate.getDate() + "/"
		                + (currentdate.getMonth()+1)  + "/" 
		                + currentdate.getFullYear() + " @ "  
		                + currentdate.getHours() + ":"  
		                + currentdate.getMinutes() + ":" 
		                + currentdate.getSeconds();
		var numberNote;
		var note = {
		 	title : document.getElementById('remember').value,
		 	text : document.getElementById('textR').value,
		 	date : datetime
		};
		table = localStorage.getItem("1");
		if(!table) {
			table = [];
			localStorage.setItem("1", JSON.stringify(table));
		} else {
			var table = JSON.parse(table);
		}
		table.push(note);
		localStorage.setItem("1", JSON.stringify(table));
		document.getElementById('remember').value="";
		document.getElementById('textR').value="";
 	}
 	displayItems();	
}

function displayItems() {
	 document.getElementById("container").innerHTML;
	 var note = "";
	 var jsonItems = localStorage.getItem("1");
	 if(jsonItems != null) {
	 var jsonItems = JSON.parse(jsonItems);
	 for(var i = jsonItems.length-1 ; i >=0 ; i--)
	 {	 	
	 	var id = "id='" + i + "'";
	     note += "<div id='note' class='border'><div id='noteText'><h2>"+jsonItems[i].title+
	    " </h2> <p>added : "+jsonItems[i].date+"</p> " +
	     " <h3>"+jsonItems[i].text+"</h3> "+
	     " </div> <div id='remove'> <div id='horizontal-seprator'></div> <button onClick='deleteItem(this)' class='delete' "+ id + ">X</button> "+
	     " </div></div><br/>";
	 }
	document.getElementById("container").innerHTML = note;
	}
}

function deleteItem(element) 
{
	var table = localStorage.getItem("1");
	table = JSON.parse(table);
	var index = table.indexOf(element.id);
	table.splice(element.id,1);
	localStorage.setItem("1", JSON.stringify(table))
	displayItems();
}