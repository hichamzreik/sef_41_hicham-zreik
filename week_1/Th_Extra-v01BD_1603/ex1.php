#!/usr/bin/php
<?php

function ListIn($dir,$prefix = '') #variable prefix for complete path
{
	 $result = array();
	 $h = opendir($dir);
	 while (($file = readdir($h)) !== false)# file in folder
	 {
	 	if ($file !== '.' and $file !== '..')
	 	{
	 		 if (is_dir("$dir/$file")) 
	 		 {
	 		 	$result = array_merge($result, ListIn("$dir/$file", "$prefix$file/"));
	 		 }
	 		 else
	 		 {
	 		 	$result[]=$prefix.$file;
	 		 }
	 	}
	 }
	 closedir($h);
  	 return $result;
}

$directory = "/".$argv[2] ;
$array = ListIn($directory);
foreach ($array as $file)
{
	echo $file."\n";
}
?>


