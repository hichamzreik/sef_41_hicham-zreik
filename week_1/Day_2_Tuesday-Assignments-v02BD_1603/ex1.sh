#!/bin/bash

echo -n >/home/$(whoami)/Desktop/log_dump.csv

for loadfile in  /var/log/*.log 
do		
	file=$(basename $loadfile)
	size=` du -h $loadfile | awk '{print $1}' `
	resultat=$file" , "$size
	echo $resultat >>/home/$(whoami)/Desktop/log_dump.csv	
done

