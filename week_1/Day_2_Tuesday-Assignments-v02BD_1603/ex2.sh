#!/bin/bash

ramGood=true
diskGood=true

perUsedRam=$(free -m | awk 'NR==2' | awk '{print ($3*100)/$2}') #percent of used Ram
perUsedRam=${perUsedRam%.*} # percent in int
if [ $perUsedRam -ge 80 ]
then
	echo "ALARM : Virtual Memory is at "$perUsedRam
	ramGood=false
fi

t=($(df -h | awk 'NR!=1' | awk '{print $1}')) #table contain names of content disk
count=0
ligne=2 
for v in ${t[*]} # v is variable
do
perUsedDisk=$(df -h | awk "NR==$ligne" | awk '{print ($3*100)/$2}')
perUsedDisk=${perUsedDisk%.*}
if [ $perUsedDisk -ge 80 ]
then
	printf "ALARM : Disk %s is at %d%%.\n" ${t[count]} $perUsedDisk
	diskGood=false
fi
((ligne++))
((count++))
done

if [ "$ramGood" = true -a "$diskGood"=true ] ;
then 
		echo "Everythink is Ok!"
fi


